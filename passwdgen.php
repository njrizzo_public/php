<?php

function lineFeed()
{
	if (php_sapi_name() == "cli")
		return "\n";
	else
		return "<BR>\n";
}


function getPassword($len, $num=0, $upper=0, $special=0)
{
	$chars= array (	
				"abcdefghijklmnopqrstuvwxyz",
				"ABCDEFGHIJKLMNOPQRSTUVWXYZ",
				"@!#$%&-+_:[]{}?|*<>/\\",
				"0123456789");
	$LOWER = 0;
	$UPPER = 1;
	$SPECIAL = 2;
	$NUMBER = 3;

	$passwd = "";
	while ( $len )
	{
		if ( $num && $upper && $special )
		{
			$charType = mt_rand(0,3);
			$maxLen = strlen($chars[$charType])-1;
			$charChoice = mt_rand(0,$maxLen);
			$passwd = $passwd . $chars[$charType][$charChoice];
		}
			
		$len --;
	}
	if (php_sapi_name() != "cli") 
		$passwd = htmlspecialchars($passwd);
	return $passwd;
}

	echo ("Strong Password Generator");
	echo lineFeed();
	echo ("Will generate a password with 20 characters with case, number and special characters");
	echo lineFeed();
	echo("Password: <FONT COLOR=BLUE> " . getPassword(20,1,1,1) . " </FONT>");
	echo lineFeed();
?>
